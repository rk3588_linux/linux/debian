#!/bin/bash

export USER=${USER:-$(id -un)}
export HOME=${HOME:-~}
export TERM=${TERM:-linux}
